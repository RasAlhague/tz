#![deny(clippy::pedantic)]
pub mod timezone;
pub mod convert;

extern crate dirs;

use chrono::prelude::*;
use structopt::StructOpt;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::prelude::*;
use self::convert::TzConverter;
use self::timezone::{Tz, Hemisphere};

pub fn run(settings: &Settings) {
    if let Some(mut doc_dir) = dirs::document_dir() {
        doc_dir.push("timezones.json");

        let doc_dir = &doc_dir.into_os_string().into_string().unwrap();

        let converter = match TzConverter::new(settings.time, doc_dir) {
            Ok(x) => x,
            Err(err) => {
                println!("Could not create converter: {}", err);
    
                return;
            },
        };
    
        if let Some(conv_date) = converter.convert(&settings.timezone) {       
            println!(
                "\nYour time: {:>02}.{:>02}.{:>02} {:>02}:{:>02}:{:>02}",
                conv_date.day(),
                conv_date.month(),
                conv_date.year(),
                conv_date.hour(),
                conv_date.minute(),
                conv_date.second()
            );
        }
        else {
            println!("\nCould not convert the date to your local timezone!");
        }
    }
    else {
        println!("\nCould not get the appdata path!");
    }
}

#[derive(StructOpt)]
#[structopt(
    about = "A custom timezone converter tool.\nSupported timezones are: EST, EDT, UTC, MEST, CEST, CEDT, CET, PST and PDT.\nSample time: 2020-04-09T0:00:00. "
)]
pub struct Settings {
    #[structopt(
        short = "t",
        long,
        help = "The date and the time that should be convertet to localtime"
    )]
    time: NaiveDateTime,
    #[structopt(
        short = "T",
        long,
        help = "The timezone of the time which should be converter to local time"
    )]
    timezone: String,
}

impl Settings {
    #[must_use]
    pub fn new() -> Settings {
        Settings::from_args()
    }

    #[must_use]
    pub fn time(&self) -> NaiveDateTime {
        self.time
    }

    #[must_use]
    pub fn timezone(&self) -> String {
        self.timezone.clone()
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TzContainer {
    pub timezones: Vec<Tz>,
}

impl TzContainer {
    /// # Errors
    pub fn from_file(file_path: &str) -> Result<TzContainer, String> {
        let mut file = match File::open(file_path){
            Ok(f) => f,
            Err(err) => return Err(format!("Could not open file: {}", err)),
        };
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();

        let container: TzContainer = match serde_json::from_str(contents.as_str()) {
            Ok(x) => x,
            Err(e) => {
                println!("Failed to load timezones: {}", e);
    
                return Err(String::from("Could not load timezones."));
            }
        };

        Ok(container)
    }

    /// # Errors
    pub fn to_file(&self, file_path: &str) -> Result<(), String> {
        let mut file = match File::create(file_path) {
            Ok(f) => f,
            Err(err) => return Err(format!("Could not create file: {}", err)),
        };
        file.write_all(serde_json::to_string(&self).unwrap().as_bytes()).unwrap();

        Ok(())
    }

    #[must_use]
    pub fn get_timezone(&self, file_name: &str) -> Option<Tz> {
        for tz in &self.timezones {
            if tz.name().to_lowercase() == file_name.to_lowercase() {
                return Some(tz.clone())
            }
        }

        None
    } 

    #[must_use]
    pub fn new_default() -> TzContainer {
        let mut timezones = Vec::new();
        timezones.push(Tz::new("incest", 69, Hemisphere::West, Some(String::from("SWEET HOME ALABAMA"))));
        timezones.push(Tz::new("utc", 0, Hemisphere::East, Some(String::from("THIS IS NOT A FUCKING TIMEZONE!!!"))));
        timezones.push(Tz::new("gmt", 0, Hemisphere::East, Some(String::from("Now this is a timetzone and not utc. If you don't know the difference then look it up idiot!"))));
        timezones.push(Tz::new("est", 5, Hemisphere::West, None));
        timezones.push(Tz::new("edt", 4, Hemisphere::West, None));
        timezones.push(Tz::new("mest", 2, Hemisphere::East, None));
        timezones.push(Tz::new("cest", 2, Hemisphere::East, None));
        timezones.push(Tz::new("cedt", 2, Hemisphere::East, None));
        timezones.push(Tz::new("cet", 1, Hemisphere::East, None));
        timezones.push(Tz::new("pst", 8, Hemisphere::West, None));
        timezones.push(Tz::new("pdt", 7, Hemisphere::West, None));

        TzContainer {
            timezones,
        }
    }
}