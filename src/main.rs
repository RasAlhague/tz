use tz::Settings;

fn main() {
    let settings = Settings::new();

    tz::run(&settings);
}
