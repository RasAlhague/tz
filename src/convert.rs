use chrono::prelude::*;
use super::TzContainer;
use super::timezone::Hemisphere;

const HOUR: i32 = 3600;

pub struct TzConverter {
    time: NaiveDateTime,
    container: TzContainer,
}

impl TzConverter {
    /// # Errors
    pub fn new(time: NaiveDateTime, tz_filepath: &str) -> Result<TzConverter, String> {
        if !std::path::Path::new(tz_filepath).exists() {
            let tz_container = TzContainer::new_default();

            match tz_container.to_file(tz_filepath) {
                Ok(x) => x,
                Err(err) => {
                    return Err(format!("Error while loading tz file: {}!", err));
                },
            };
        }

        let container = match TzContainer::from_file(tz_filepath) {
            Ok(x) => x,
            Err(err) => {
                return Err(format!("Error while loading tz file: {}!", err));
            },
        };

        Ok(TzConverter { time, container })
    }

    #[must_use]
    pub fn convert(&self, timezone: &str) -> Option<DateTime<Local>> {
        if let Some(tz) = self.container.get_timezone(timezone) {
            println!("Target timezone: {}", tz.name());
            println!("Offset: {}{:?}", tz.hemisphere(), tz.offset());

            if let Some(i) = tz.info() {
                println!("Info: {}", i);
            };

            if tz.offset() > 24 || tz.offset() < -24 {
                return None;
            }

            let offset = match tz.hemisphere() {
                Hemisphere::East => FixedOffset::east(tz.offset() * HOUR),
                Hemisphere::West => FixedOffset::west(tz.offset() * HOUR),
            };

            let new_time = self.time - offset;

            return Some(Local.from_utc_datetime(&new_time));
        }

        None
    }
}