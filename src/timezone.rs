use serde::{Deserialize, Serialize};
use std::fmt::Display;
use std::fmt;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Tz {
    name: String,
    offset: i32,
    hemisphere: Hemisphere,
    info: Option<String>,
}

impl Tz {
    #[must_use]
    pub fn new(name: &str, offset: i32, hemisphere: Hemisphere, info: Option<String>) -> Tz {
        Tz {
            name: String::from(name),
            offset,
            hemisphere,
            info,
        }
    }

    #[must_use]
    pub fn name(&self) -> String {
        self.name.clone()
    }

    #[must_use]
    pub fn offset(&self) -> i32 {
        self.offset
    }

    #[must_use]
    pub fn hemisphere(&self) -> Hemisphere {
        self.hemisphere.clone()
    }

    #[must_use]
    pub fn info(&self) -> Option<String> {
        self.info.clone()
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum Hemisphere {
    East,
    West,
}

impl Display for Hemisphere {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Hemisphere::East => write!(f, "+"),
            Hemisphere::West => write!(f, "-"),
        }
    }
}